package com.example.totalvocalcommander.Utils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.Pair;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class OverlayServiceHelper {
    private static final String TAG = "OverlayServiceHelper";
    // We use the same layout parameters for all overlay objects
    private final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
            WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
            PixelFormat.TRANSLUCENT);
    /*
        Special object overlay made only for the Learn Mode
        This overlay consumes touch this why it has to be different from the previous one
     */
    private final WindowManager.LayoutParams paramsForLearnMode = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
            WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
            PixelFormat.TRANSLUCENT);
    private Context mContext;
    private List<String> mHashData;
    private List<Pair<TextView, WindowManager.LayoutParams>> mTextViews;
    private List<Pair<RectangleView, WindowManager.LayoutParams>> mRectangleViews;
    private int mTextNr = 0;
    private WindowManager mWindowManager;
    private boolean mAdvancedMode = false;
    private GestureOverlay mGestureOverlay;
    private VoiceAssistView mRectangleVoiceAssist;

    public OverlayServiceHelper(Context context, WindowManager windowManager) {
        mWindowManager = windowManager;
        mContext = context;
        mHashData = new ArrayList<>();
        mTextViews = new ArrayList<>();
        mRectangleViews = new ArrayList<>();
        mGestureOverlay = new GestureOverlay(context);
    }

    /*
        Create the number object over view item and updates the overlay only if user turned on the
        advanced mode
     */
    public void updateOverlayClickable(int x, int y) {
        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams();
        parameters.copyFrom(params);
        parameters.gravity = Gravity.START | Gravity.TOP;
        parameters.x = x;
        parameters.y = y;
        TextView textView = new TextView(mContext);
        textView.setText(String.valueOf(mTextNr));
        textView.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_dark));
        textView.setTextSize(20f);
        Pair<TextView, WindowManager.LayoutParams> item = new Pair<TextView, WindowManager.LayoutParams>(textView, parameters);
        mTextViews.add(item);
        if (mAdvancedMode) {
            mWindowManager.addView(item.first, item.second);
        }
        mTextNr++;
    }

    public void setAdvancedMode(boolean advancedMode) {
        mAdvancedMode = advancedMode;
    }

    /*
        Rectangle object that is showed over scrollable and is show only
        if advanced mode is turned on
     */
    public void updateOverlayScrollable(Rect r, int color, String colorString) {

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams();
        parameters.copyFrom(params);
        RectangleView rectangleView = new RectangleView(mContext, r, color, colorString);
        Pair<RectangleView, WindowManager.LayoutParams> item = new Pair<RectangleView, WindowManager.LayoutParams>(rectangleView, parameters);
        boolean exists = false;
        for (Pair<RectangleView, WindowManager.LayoutParams> itemRect:mRectangleViews) {
         if(itemRect.first.equal(item.first.mRect)){
             exists = true;
         }
        }

        if(!exists){
            mRectangleViews.add(item);
            if (mAdvancedMode) {
                mWindowManager.addView(item.first, item.second);
            }
        }


    }

    public void updateOverlayTTSData(Rect rect) {
        mRectangleVoiceAssist = new VoiceAssistView(mContext, rect);
        mWindowManager.addView(mRectangleVoiceAssist, params);

    }

    public void removeLastRectangleTTS() {
        try {
            mWindowManager.removeView(mRectangleVoiceAssist);
        } catch (Exception e) {
        }
    }

    public void resetImageViews() {
        hideImageViews();
        mHashData.clear();
        mTextViews.clear();
        mTextNr = 0;
    }

    public void resetScrollViews() {
        hideScrollViews();
        mRectangleViews.clear();
    }

    public void hideImageViews() {
        for (Pair<TextView, WindowManager.LayoutParams> item : mTextViews) {
            try {
                mWindowManager.removeView(item.first);
            } catch (IllegalArgumentException e) {

            }

        }
    }

    public void hideScrollViews() {
        for (Pair<RectangleView, WindowManager.LayoutParams> item : mRectangleViews) {
            try {
                mWindowManager.removeView(item.first);
            } catch (IllegalArgumentException e) {

            }
        }

    }

    public void showImageViews() {
        for (Pair<TextView, WindowManager.LayoutParams> item : mTextViews) {
            mWindowManager.addView(item.first, item.second);
        }
    }

    public void showScrollViews() {
        for (Pair<RectangleView, WindowManager.LayoutParams> item : mRectangleViews) {
            mWindowManager.addView(item.first, item.second);
        }
    }

    public boolean mapContains(String xy) {
        return mHashData.contains(xy);
    }

    public void addXYToMap(String xy) {
        mHashData.add(xy);
    }

    public void addGesturelayer() {
        mWindowManager.addView(mGestureOverlay, paramsForLearnMode);
    }

    public void saveGesture(String name) {
        mGestureOverlay.saveEvents(name);
        mGestureOverlay.clear();
        mWindowManager.removeView(mGestureOverlay);
    }

    public void deleteOverlay() {
        mGestureOverlay.clear();
        mWindowManager.removeView(mGestureOverlay);
    }
}
