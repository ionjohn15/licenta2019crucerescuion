package com.example.totalvocalcommander.Utils;

import android.accessibilityservice.GestureDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.totalvocalcommander.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.ContentValues.TAG;
import static android.content.Context.WINDOW_SERVICE;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_CLEAR;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_OFF;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_ON;

/*
    Internal class used as main helper for the main service that is intended to keep the main class as clean as possible

 */
public class ListenerServiceHelper {
    private static final int SCROLL_PADDING = 150;

    private Context mContext;
    //mRectData is used to evade duplicates on the main screen
    private List<Rect> mRectData;
    //mData list of total visible and clickable items on the screen
    private List<ItemModel> mData;

    private List<ItemModel> mCacheData;
    private int currentColor = 0;
    //hash map for all scrollable nodes on the current display
    private HashMap<String, AccessibilityNodeInfo> mHashData;
    private String[] mColorNames;
    private WindowManager mWindowManager;
    private AccessibilityNodeInfo mCurrentEditText;
    private String mAppendString;
    private boolean mVoiceAssist = false;
    private boolean mDictateModeChanged = false;
    private boolean mDictateMode = false;
    private List<EventModel> mEventModels;
    private boolean mCanSendVoiceData = true;
    private boolean mJustTurnedOn = false;

    public ListenerServiceHelper(Context context) {
        mRectData = new ArrayList<>();
        mData = new ArrayList<>();
        mContext = context;
        mColorNames = context.getResources().getStringArray(R.array.colorNames);
        mHashData = new HashMap<>();
        mAppendString = "";
        mEventModels = new ArrayList<>();
        mCacheData = new ArrayList<>();
        mWindowManager = (WindowManager) mContext.getSystemService(WINDOW_SERVICE);
    }

    public boolean ismDictateModeChanged() {
        return mDictateModeChanged;
    }

    public boolean ismDictateMode() {
        return mDictateMode;
    }

    public void setmDictateMode(boolean mDictateMode) {
        this.mDictateMode = mDictateMode;
        mAppendString = "";
    }

    /*
        Method used to send info about clickable views to the OverlayService
     */
    public void sendIsClickable(AccessibilityNodeInfo rootNode, int id) {

        Display display = mWindowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        Rect rect = new Rect();
        rootNode.getBoundsInScreen(rect);
        if (size.x < rect.centerX() || size.y < rect.centerY()) {
            return;
        }
        Intent sendLevel = new Intent();
        sendLevel.setAction("OBJECTS_DATA");
        sendLevel.putExtra("X", rect.centerX());
        sendLevel.putExtra("Y", rect.centerY());
        sendLevel.putExtra("id", id);
        mContext.sendBroadcast(sendLevel);
        ItemModel itemModel = new ItemModel(rootNode, rect);
        mData.add(itemModel);
    }

    public void setmVoiceAssist(boolean mode) {
        mVoiceAssist = mode;
        if (mode) {
            mJustTurnedOn = true;
            triggerVoiceAssistantCheck();
        }
    }

    /*
            Method used to send info about scrollable views to the OverlayService
         */
    public void sendIsScrollable(AccessibilityNodeInfo rootNode, int id) {
        Rect rect = new Rect();
        rootNode.getBoundsInScreen(rect);
        if (!mRectData.contains(rect)) {
            mRectData.add(rect);
            Intent sendLevel = new Intent();
            sendLevel.setAction("SCROLLABLE_DATA");
            sendLevel.putExtra("r", rect);
            sendLevel.putExtra("id", id);
            sendLevel.putExtra("colorString", mColorNames[currentColor].toLowerCase());
            mHashData.put(mColorNames[currentColor].toLowerCase(), rootNode);
            currentColor++;
            mContext.sendBroadcast(sendLevel);
        }
    }

    public AccessibilityNodeInfo getScrollNodeForColor(String color) {
        return mHashData.get(color.toLowerCase());
    }

    /*
    Reset all visible data structure
     */
    public void resetData() {
        mData.clear();
        mRectData.clear();
        currentColor = 0;
    }

    public void triggerVoiceAssistantCheck() {
        mCanSendVoiceData = false;
        for (ItemModel itemPD : mData) {
            boolean exists = false;
            for (ItemModel itemCD : mCacheData) {
                if (itemPD.equals(itemCD)) {
                    exists = true;
                }
            }

            if (!exists) {
                mCanSendVoiceData = true;
                break;
            }
        }
        if (mCanSendVoiceData && mVoiceAssist || mJustTurnedOn && mVoiceAssist) {
            Intent sendLevel2 = new Intent();
            sendLevel2.setAction("VOICE_RESET");
            mContext.sendBroadcast(sendLevel2);
            sendVoiceAssistantData();
            mJustTurnedOn = false;
        }
        mCacheData = new ArrayList<>(mData);
    }

    public void sendVoiceAssistantData() {
        for (ItemModel item : mData) {
            Intent sendLevel2 = new Intent();
            sendLevel2.setAction("VOICE_DATA");
            sendLevel2.putExtra("voice_data", item.getContentDescritor());
            sendLevel2.putExtra("rect", item.getRect());
            mContext.sendBroadcast(sendLevel2);
        }

    }

    public boolean isViewIdValid(int viewId) {
        return mData.size() != 0 && mData.size() >= viewId && mData.get(viewId) != null;
    }

    public ItemModel itemWithThisContent(String pressWord) {
        for (ItemModel item : mData) {
            if (item.containsString(pressWord)) {
                return item;
            }
        }
        return null;
    }

    public void setmCurrentEditText(AccessibilityNodeInfo editText) {
        mCurrentEditText = editText;
    }

    /*
    Method used to populate an EditText with a string data
     */
    public void appendStringToEdittext(String text) {
        Log.w(TAG, mCurrentEditText.getClassName() + " ");
        if (mCurrentEditText.getClassName().equals("android.widget.EditText")) {
            if (mAppendString.isEmpty()) {
                mAppendString += text;
            } else {
                mAppendString += " " + text;
            }

            if (text.equals("dictate clear")) {
                mAppendString = "";
            }
            Bundle arguments = new Bundle();
            arguments.putCharSequence(AccessibilityNodeInfo
                    .ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, mAppendString);
            mCurrentEditText.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
        }
    }


    /*
    Creates a gesture object used for dispatch method in main service
     */
    public GestureDescription createClick(int viewId, boolean longPress, String pressWord) {
        // for a single tap a duration of 1 ms is enough

        int DURATION = 1;
        if (longPress) {
            DURATION = 2000;
        }
        Path clickPath = new Path();
        if (!pressWord.equals("")) {
            ItemModel itemModel = itemWithThisContent(pressWord);
            if (itemModel != null) {
                Log.w("debug", "found the item");
                clickPath.moveTo(itemModel.getX(), itemModel.getY());
            } else {
                return null;
            }
        } else {
            if (mData.size() <= viewId) {
                return null;
            }
            clickPath.moveTo(mData.get(viewId).getX(), mData.get(viewId).getY());
        }


        GestureDescription.StrokeDescription clickStroke =
                new GestureDescription.StrokeDescription(clickPath, 0, DURATION);
        GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
        clickBuilder.addStroke(clickStroke);
        return clickBuilder.build();
    }

    public void sendDataForGesture(EventModel eventModel) {
        mEventModels.add(eventModel);
    }

    public GestureDescription creatGestureTap(EventModel eventModel) {
        Log.w("ddebug", "creatGestureTap " + eventModel.getTotalTime());
        long DURATION = eventModel.getTotalTime();
        Path clickPath = new Path();
        clickPath.moveTo(eventModel.getX(), eventModel.getY());
        GestureDescription.StrokeDescription clickStroke =
                new GestureDescription.StrokeDescription(clickPath, 0, DURATION <= 0 ? 1 : DURATION);
        GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
        clickBuilder.addStroke(clickStroke);
        return clickBuilder.build();
    }

    public GestureDescription createGestureSwipe(Path path, long duration) {
        Log.w("ddebug", "createGestureSwipe " + duration);
        GestureDescription.StrokeDescription clickStroke =
                new GestureDescription.StrokeDescription(path, 0, duration <= 0 ? 1 : duration);
        GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
        clickBuilder.addStroke(clickStroke);
        return clickBuilder.build();
    }

    public List<GestureDescription> createGesures() {
        List<GestureDescription> returnArray = new ArrayList<>();
        boolean lastIsTap = true;
        Path swipePath = new Path();
        boolean newPath = true;
        long duration = 1000;
        for (EventModel item : mEventModels) {
            if (item.getIsTap() != lastIsTap && item.getIsTap()) {
                newPath = true;
                Log.w("ddebug", "duration #1 " + duration);
                GestureDescription gestureDescription = createGestureSwipe(swipePath, duration);
                returnArray.add(gestureDescription);
            }
            if (item.getIsTap()) {
                GestureDescription gestureDescription = creatGestureTap(item);
                returnArray.add(gestureDescription);
            } else if (newPath) {
                swipePath = new Path();
                swipePath.moveTo(item.getX(), item.getY());
                newPath = false;
            } else {
                swipePath.lineTo(item.getX(), item.getY());
                duration = item.getTotalTime();
                Log.w("ddebug", "duration #2 " + duration);
            }
            lastIsTap = item.getIsTap();
        }

        if (!lastIsTap) {
            GestureDescription gestureDescription = createGestureSwipe(swipePath, duration);
            returnArray.add(gestureDescription);
        }
        mEventModels.clear();
        return returnArray;
    }

    /*
        Object for dispatch the Enter butotn on the keyboard as there is no enter
        action api for softkeboard  in android
     */
    public GestureDescription createEnterClick() {
        setmDictateMode(false);
        // for a single tap a duration of 1 ms is enough
        final int DURATION = 1;
        Path clickPath = new Path();
        Display display = mWindowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int x;
        int y;
        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            x = size.x - 50;
            y = size.y - 100;
        } else {
            x = size.x - 100;
            y = size.y - 20;
        }

        clickPath.moveTo(x, y);
        GestureDescription.StrokeDescription clickStroke =
                new GestureDescription.StrokeDescription(clickPath, 0, DURATION);
        GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
        clickBuilder.addStroke(clickStroke);
        return clickBuilder.build();
    }

    public void resetJustChangedDictateMode() {
        mDictateModeChanged = false;
    }

    /*
        Dictate decision maker
     */
    public void dictateAction(int id) {

        switch (id) {
            case DICTATE_ON:
                setmDictateMode(true);
                mDictateModeChanged = true;
                break;
            case DICTATE_OFF:
                setmDictateMode(false);
                mDictateModeChanged = true;
                break;
            case DICTATE_CLEAR:
                mAppendString = "";
                break;
            default:
                Log.d(TAG, "default");

        }
    }

    /*
        Gesture object used to simulate a swipe action
     */
    public GestureDescription createSwipe(String color, boolean startToEnd, boolean xAxis) {
        // for a single tap a duration of 1 ms is enough
        final int DURATION = 1000;
        Path swipePath = new Path();
        Rect rect = new Rect();
        AccessibilityNodeInfo currentNode = getScrollNodeForColor(color);
        if (currentNode == null) {
            return null;
        }
        currentNode.getBoundsInScreen(rect);
        if (startToEnd) {
            if (xAxis) {
                swipePath.moveTo(rect.right - SCROLL_PADDING, rect.centerY());
                swipePath.lineTo(rect.left + SCROLL_PADDING, rect.centerY());
            } else {
                swipePath.moveTo(rect.centerX(), rect.bottom - SCROLL_PADDING);
                swipePath.lineTo(rect.centerX(), rect.top + SCROLL_PADDING);
            }
        } else {
            if (xAxis) {
                swipePath.moveTo(rect.left + SCROLL_PADDING, rect.centerY());
                swipePath.lineTo(rect.right - SCROLL_PADDING, rect.centerY());
            } else {
                swipePath.moveTo(rect.centerX(), rect.top + SCROLL_PADDING);
                swipePath.lineTo(rect.centerX(), rect.bottom - SCROLL_PADDING);
            }
        }
        GestureDescription.StrokeDescription clickStroke =
                new GestureDescription.StrokeDescription(swipePath, 0, DURATION);
        GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
        clickBuilder.addStroke(clickStroke);
        return clickBuilder.build();
    }
}
