package com.example.totalvocalcommander;


import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.example.totalvocalcommander.Utils.DecisionMaker;
import com.example.totalvocalcommander.Utils.EventModel;
import com.example.totalvocalcommander.Utils.ListenerServiceHelper;

import java.util.List;

import static android.content.ContentValues.TAG;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_ENTER;
import static com.example.totalvocalcommander.Utils.DecisionMaker.WORD_VALUE;

/*
    Main service that has most of the permissisons
    It extracts data from views and listens to content changes
    It dispatches all the gesture events
 */
public class ListenerService extends AccessibilityService {
    private static final int TIME_TO_WAIT = 300;
    private static final int FIRST_INIT = 1000;
    private static final int NORMAL_STATE = 1;
    private static final int GESTURES_LOADING = 2;
    private static Handler myHandler = new Handler();
    private int mId = 0;
    private ListenerServiceReceiver mReceiver;
    private DecisionMaker mDecisionMaker;
    private ListenerServiceHelper mListenerServiceHelper;
    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            AccessibilityNodeInfo rootNode;
            rootNode = getRootInActiveWindow();
            if (rootNode != null) {
                exploreChilds(rootNode);
                mListenerServiceHelper.triggerVoiceAssistantCheck();
            }

            mId++;
        }
    };
    private List<GestureDescription> gestureDescriptions;
    private int gestureDescriptionsIterator;
    private int STATE = NORMAL_STATE;
    // callback invoked either when the gesture has been completed or cancelled
    GestureResultCallback callback = new AccessibilityService.GestureResultCallback() {
        @Override
        public void onCompleted(GestureDescription gestureDescription) {
            super.onCompleted(gestureDescription);
            Log.d(TAG, "gesture completed");
            if (STATE == GESTURES_LOADING) {
                gestureDescriptionsIterator++;
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nextCustomGesture();
                    }
                }, 500);

            }
        }

        @Override
        public void onCancelled(GestureDescription gestureDescription) {
            super.onCancelled(gestureDescription);
            Log.d(TAG, "gesture cancelled");
        }

    };

    @Override
    public void onCreate() {
        super.onCreate();
        mDecisionMaker = new DecisionMaker();
        gestureDescriptionsIterator = 0;
        mListenerServiceHelper = new ListenerServiceHelper(this);
        mReceiver = new ListenerServiceReceiver();
        registerReceiver(mReceiver, new IntentFilter("PRESS_DATA"));
        registerReceiver(mReceiver, new IntentFilter("VOICE_ASSIST"));
        registerReceiver(mReceiver, new IntentFilter("LEARN_MODE_DISPATCH"));
        registerReceiver(mReceiver, new IntentFilter("TRY_GESTURE"));
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityNodeInfo source = accessibilityEvent.getSource();
        if (source == null) {
            return;
        }
        if (accessibilityEvent.getEventType() == AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED) {
            restart(TIME_TO_WAIT);
        }
    }

    /*
        Restarts the timer on data updating thread
     */
    public void restart(int timeToWait) {
        myHandler.removeCallbacks(mRunnable);
        mListenerServiceHelper.resetData();
        myHandler.postDelayed(mRunnable, timeToWait);

    }

    /*
        Method that is receives the main information from the listener method and distributes it to
        the overlay service
     */
    public void exploreChilds(AccessibilityNodeInfo rootNode) {
        if (rootNode == null) {
            return;
        }
        if (rootNode.isClickable()) {
            mListenerServiceHelper.sendIsClickable(rootNode, mId);
        }

        if (rootNode.isScrollable()) {
            mListenerServiceHelper.sendIsScrollable(rootNode, mId);
        }
        if (rootNode.isFocused() && rootNode.getClassName().equals("android.widget.EditText")) {
            mListenerServiceHelper.setmCurrentEditText(rootNode);
        }

        for (int i = 0; i < rootNode.getChildCount(); i++) {
            if (rootNode.getChildCount() > 0) {
                exploreChilds(rootNode.getChild(i));
            }
        }

    }

    /*
        Methods used to simulate a custom gesture
     */
    private void startCustomGesture() {
        if (gestureDescriptions.size() > 0) {
            boolean result = this.dispatchGesture(gestureDescriptions.get(0), callback, null);
            STATE = GESTURES_LOADING;
        } else {
            STATE = NORMAL_STATE;
            gestureDescriptionsIterator = 0;
            gestureDescriptions.clear();
        }
    }

    private void nextCustomGesture() {
        if (gestureDescriptions.size() > gestureDescriptionsIterator) {
            boolean result = this.dispatchGesture(gestureDescriptions.get(gestureDescriptionsIterator), callback, null);
            STATE = GESTURES_LOADING;
        } else {
            STATE = NORMAL_STATE;
            gestureDescriptionsIterator = 0;
            gestureDescriptions.clear();
        }
    }

    /*
    Main receiver that communicates with other services via broadcast
     */
    class ListenerServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("PRESS_DATA")) {
                int viewId = intent.getIntExtra("view_value", -1);
                String direction = intent.getStringExtra("direction");
                String color = intent.getStringExtra("color");
                String string_data = intent.getStringExtra("string_data");
                String pressWord = intent.getStringExtra("press_word");
                boolean forced = intent.getBooleanExtra("forced", false);
                boolean longPress = intent.getBooleanExtra("long_press", false);

                if (direction != null && color != null && !direction.equals("none") && !color.equals("none")) {
                    color = color.toLowerCase();
                    boolean performed;
                    if (forced) {
                        if (mDecisionMaker.validDirection(direction)) {
                            GestureDescription gestureDescription = mListenerServiceHelper.createSwipe(color, mDecisionMaker.getStartToEnd(direction),
                                    mDecisionMaker.getisXAxisForDirection(direction));
                            if (gestureDescription != null) {
                                performed = dispatchGesture(gestureDescription, callback, null);
                            } else {
                                performed = false;
                            }

                        } else {
                            performed = false;
                        }
                    } else {
                        performed = mDecisionMaker.performScrollAction(mListenerServiceHelper.getScrollNodeForColor(color), direction);
                    }
                    mListenerServiceHelper.setmDictateMode(false);
                    Log.d(TAG, "Scroll action: color " + color + " direction " + direction + " return value " + performed);
                }
                if (viewId >= 0) {
                    if (mListenerServiceHelper.isViewIdValid(viewId) || viewId == WORD_VALUE) {
                        GestureDescription gestureDescription = mListenerServiceHelper.createClick(viewId, longPress, pressWord);
                        boolean result = false;
                        if (gestureDescription != null) {
                            result = dispatchGesture(gestureDescription, callback, null);
                        }
                        Log.d(TAG, "Gesture dispatched? " + result);
                    }
                    mListenerServiceHelper.setmDictateMode(false);
                }
                if (viewId < -1 && mDecisionMaker.validateGlobalAction(viewId)) {
                    mListenerServiceHelper.setmDictateMode(false);
                    performGlobalAction(mDecisionMaker.getGlobalAction(viewId));
                }
                mListenerServiceHelper.resetJustChangedDictateMode();
                if (mDecisionMaker.isDictateId(viewId)) {
                    if (viewId == DICTATE_ENTER && getSoftKeyboardController().getShowMode() != AccessibilityService.SHOW_MODE_HIDDEN) {
                        GestureDescription gestureDescription = mListenerServiceHelper.createEnterClick();
                        if (gestureDescription != null) {
                            boolean performed = dispatchGesture(gestureDescription, callback, null);
                            Log.w(TAG, "dictate enter " + performed);
                        }
                        mListenerServiceHelper.setmDictateMode(false);
                    } else {
                        mListenerServiceHelper.dictateAction(viewId);
                    }
                }
                if (mListenerServiceHelper.ismDictateMode() && !mListenerServiceHelper.ismDictateModeChanged()) {
                    if (string_data != null) {
                        mListenerServiceHelper.appendStringToEdittext(string_data);
                    }
                }


                Log.w(TAG, viewId + " ####");
            }
            if (intent.getAction().equals("VOICE_ASSIST")) {
                boolean voiceAssist = intent.getBooleanExtra("voice_mode", false);
                mListenerServiceHelper.setmVoiceAssist(voiceAssist);
            }
            if (intent.getAction().equals("LEARN_MODE_DISPATCH")) {
                float x = intent.getFloatExtra("x", -1);
                float y = intent.getFloatExtra("y", -1);
                long time = intent.getLongExtra("time", 1000);
                boolean isTap = intent.getBooleanExtra("is_tap", true);
                if (y != -1 && x != -1) {
                    EventModel eventModel = new EventModel(x, y, time, isTap);
                    mListenerServiceHelper.sendDataForGesture(eventModel);
                }
            }

            if (intent.getAction().equals("TRY_GESTURE")) {
                boolean isEnd = intent.getBooleanExtra("end", false);
                gestureDescriptions = mListenerServiceHelper.createGesures();
                Log.w(TAG, "try gesture " + gestureDescriptions.size());
                startCustomGesture();

            }
        }
    }


}

