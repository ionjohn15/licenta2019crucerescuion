package com.example.totalvocalcommander;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class VocalAssistService extends Service {
    private static final String TAG = "VocalAssistService";
    String mostRecentUtteranceID;
    private TextToSpeech mTextToSpeech;
    private DataForVoiceAssist mReceiver;
    private Context mContext;
    private List<Rect> mRectData;
    private int iterator = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mRectData = new ArrayList<>();
        mTextToSpeech = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    mTextToSpeech.setLanguage(Locale.UK);
                }
                if (status == TextToSpeech.SUCCESS) {
                    ttsInitialized();
                }
            }
        });


        mReceiver = new DataForVoiceAssist();
        registerReceiver(mReceiver, new IntentFilter("VOICE_DATA"));
        registerReceiver(mReceiver, new IntentFilter("VOICE_RESET"));
    }

    public void ttsInitialized() {
        mTextToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                // Speaking started.
                sendRemoveOverlayNotification();
                sendOverlayData();
            }

            @Override
            public void onDone(String utteranceId) {
                // Speaking stopped.
                sendRemoveOverlayNotification();

            }

            @Override
            public void onError(String utteranceId) {
                // There was an error.
                Log.w(TAG, "There was an error.");
            }
        });
    }

    public void sendOverlayData() {
        notifySpeechModule(false);
        Rect item = mRectData.get(iterator);
        Intent sendLevel2 = new Intent();
        sendLevel2.setAction("FROM_TTS_DATA");
        sendLevel2.putExtra("rect", item);
        sendBroadcast(sendLevel2);
        iterator++;
    }

    public void sendRemoveOverlayNotification() {
        if (iterator == mRectData.size()) {
            notifySpeechModule(true);
        }
        Intent sendLevel2 = new Intent();
        sendLevel2.setAction("FROM_TTS_RESET");
        sendBroadcast(sendLevel2);
    }

    public void notifySpeechModule(boolean mode) {
        Intent sendLevel = new Intent();
        sendLevel.setAction("SWITCH_TTS");
        sendLevel.putExtra("mode", mode);
        sendBroadcast(sendLevel);
    }

    public void speakTxt(String message) {
        mostRecentUtteranceID = (new Random().nextInt() % 9999999) + "";
        Bundle params = new Bundle();
        params.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, mostRecentUtteranceID);
        mTextToSpeech.speak(message, TextToSpeech.QUEUE_ADD, params, mostRecentUtteranceID);
    }

    public void stop() {
        mTextToSpeech.stop();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class DataForVoiceAssist extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("VOICE_DATA")) {
                String forSpeak = intent.getStringExtra("voice_data");
                Rect rect = intent.getParcelableExtra("rect");
                if (forSpeak != null && !forSpeak.isEmpty()) {
                    speakTxt(forSpeak);
                    mRectData.add(rect);
                }
            }
            if (intent.getAction().equals("VOICE_RESET")) {
                stop();
                mRectData.clear();
                iterator = 0;
            }
        }
    }
}
