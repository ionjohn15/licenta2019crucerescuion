package com.example.totalvocalcommander.Utils;

/*
    EventModel, class used for storing base MotionEvent data.
    Used to create a custom Gesture.
 */
public class EventModel {
    private float x;
    private float y;
    private long totalTime;
    private boolean isTap;

    public EventModel(float x, float y, long totalTime, boolean isTap) {
        this.x = x;
        this.y = y;
        this.totalTime = totalTime;
        this.isTap = isTap;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public boolean getIsTap() {
        return isTap;
    }

    @Override
    public String toString() {
        String returnItem = " X: " + Float.toString(x) + " Y: " + Float.toString(y) + " getTotalTime: " + Long.toString(totalTime);
        return returnItem;
    }
}
