package com.example.totalvocalcommander.Utils;

import android.util.Log;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.List;

import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_BACK;
import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_HOME;
import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_NOTIFICATIONS;
import static android.accessibilityservice.AccessibilityService.GLOBAL_ACTION_RECENTS;
import static android.content.ContentValues.TAG;

/*
    Class used to help main service ListenerService and SpeechService to make decisions regarding
    user speech outputs and actions
 */
public class DecisionMaker {
    public static final int WORD_VALUE = 9999;
    public static final int INVALID_VALUE = -1;
    public static final int HOME_ACTION = -2;
    public static final int BACK_ACTION = -3;
    public static final int NOTIFICATIONS_ACTION = -4;
    public static final int RECENT_ACTION = -5;
    public static final int DICTATE_ON = -6;
    public static final int DICTATE_OFF = -7;
    public static final int DICTATE_CLEAR = -8;
    public static final int DICTATE_ENTER = -9;
    private static final AccessibilityNodeInfo.AccessibilityAction SCROLL_UP = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP;
    private static final AccessibilityNodeInfo.AccessibilityAction SCROLL_DOWN = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN;
    private static final AccessibilityNodeInfo.AccessibilityAction SCROLL_LEFT = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT;
    private static final AccessibilityNodeInfo.AccessibilityAction SCROLL_RIGHT = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT;
    private static final AccessibilityNodeInfo.AccessibilityAction SCROLL_BACKWARD = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_BACKWARD;
    private static final AccessibilityNodeInfo.AccessibilityAction SCROLL_FORWARD = AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_FORWARD;

    public DecisionMaker() {

    }

    /*
        Method helper that decides based on AccessibilityNodeInfo action list the correct direction
        for the scroll action as some Node info may not contain the correct action list
        added by the users at implementation
     */
    public boolean performScrollAction(AccessibilityNodeInfo node, String direction) {
        if (node == null)
            return false;
        List<AccessibilityNodeInfo.AccessibilityAction> actions = node.getActionList();
        switch (direction.toUpperCase()) {
            case "UP": {
                if (actions.contains(SCROLL_UP))
                    return node.performAction(SCROLL_UP.getId());
                if (actions.contains(SCROLL_BACKWARD))
                    return node.performAction(SCROLL_BACKWARD.getId());
                return false;
            }
            case "DOWN": {
                if (actions.contains(SCROLL_DOWN))
                    return node.performAction(SCROLL_DOWN.getId());
                if (actions.contains(SCROLL_FORWARD))
                    return node.performAction(SCROLL_FORWARD.getId());
                return false;
            }
            case "LEFT": {
                if (actions.contains(SCROLL_LEFT))
                    return node.performAction(SCROLL_LEFT.getId());
                if (actions.contains(SCROLL_BACKWARD))
                    return node.performAction(SCROLL_BACKWARD.getId());
                return false;
            }
            case "RIGHT": {
                if (actions.contains(SCROLL_RIGHT))
                    return node.performAction(SCROLL_RIGHT.getId());
                if (actions.contains(SCROLL_FORWARD))
                    return node.performAction(SCROLL_FORWARD.getId());
                return false;
            }
            default: {
                Log.e(TAG, "Invalid direction");
                return false;
            }

        }

    }

    /*
        Method used to validate a string for correct scroll directions
     */
    public boolean validDirection(String direction) {
        switch (direction.toUpperCase()) {
            case "UP": {
                return true;
            }
            case "DOWN": {
                return true;
            }
            case "LEFT": {
                return true;
            }
            case "RIGHT": {
                return true;
            }
            default: {
                Log.e(TAG, "Invalid direction");
                return false;
            }

        }
    }

    /*
        Method used by the forced scroll action to convert the string to correct
        axis for the scroll action
     */
    public boolean getisXAxisForDirection(String direction) {
        switch (direction.toUpperCase()) {
            case "UP": {
                return false;
            }
            case "DOWN": {
                return false;
            }
            case "LEFT": {
                return true;
            }
            case "RIGHT": {
                return true;
            }
            default: {
                Log.e(TAG, "Invalid direction");
                return false;
            }

        }
    }

    public boolean getStartToEnd(String direction) {
        switch (direction.toUpperCase()) {
            case "UP": {
                return false;
            }
            case "DOWN": {
                return true;
            }
            case "LEFT": {
                return false;
            }
            case "RIGHT": {
                return true;
            }
            default: {
                Log.e(TAG, "Invalid getStartToEnd");
                return false;
            }

        }
    }

    /*
        Validation for custom global actions set by the developer
     */
    public boolean validateGlobalAction(int value) {
        return value >= -5 && value <= -2;
    }

    /*
        int to Global action convertor
     */
    public int getGlobalAction(int value) {
        switch (value) {
            case HOME_ACTION:
                return GLOBAL_ACTION_HOME;
            case BACK_ACTION:
                return GLOBAL_ACTION_BACK;
            case NOTIFICATIONS_ACTION:
                return GLOBAL_ACTION_NOTIFICATIONS;
            case RECENT_ACTION:
                return GLOBAL_ACTION_RECENTS;
            default:
                return GLOBAL_ACTION_HOME;
        }
    }

    /*
        Return a confirmation if the user want to perform a dictate action
     */
    public boolean isDictateId(int value) {
        switch (value) {
            case DICTATE_CLEAR:
                return true;
            case DICTATE_ENTER:
                return true;
            case DICTATE_OFF:
                return true;
            case DICTATE_ON:
                return true;
            default:
                return false;
        }
    }


}
