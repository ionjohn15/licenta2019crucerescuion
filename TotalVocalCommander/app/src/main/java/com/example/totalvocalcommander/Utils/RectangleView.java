package com.example.totalvocalcommander.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/*
    RectangleView, view object used by the OverlayService to highlight scrollable views
 */
public class RectangleView extends View {
    Paint mPaintRectangle = new Paint();
    Paint mPaintText = new Paint();
    Rect mRect;
    int mColor = Color.BLACK;
    Rect mLocalRect;
    String mColorString = "black";

    public RectangleView(Context context, Rect rect, int color, String colorString) {
        super(context);
        this.mRect = rect;
        this.mColor = color;
        this.mColorString = colorString;
        mLocalRect = new Rect();
    }
    public boolean equal(Rect r) {
        return r.left == mRect.left && r.bottom == mRect.bottom && r.top == mRect.top && r.right == mRect.right;
    }
    @Override
    public void onDraw(Canvas canvas) {
        canvas.scale(1, 1);
        mPaintRectangle.setColor(mColor);
        mPaintRectangle.setStyle(Paint.Style.STROKE);
        mPaintRectangle.setStrokeWidth(10);
        canvas.getClipBounds(mLocalRect);

        mRect.right = Math.min(mRect.right, mLocalRect.right);
        mRect.bottom = Math.min(mRect.bottom, mLocalRect.bottom);
        mRect.left = mRect.left + 10;
        mRect.right = mRect.right - 10;
        mRect.top = mRect.top + 10;
        mRect.bottom = mRect.bottom - 10;
        canvas.drawRect(mRect, mPaintRectangle);
        mPaintText.setColor(mColor);
        mPaintText.setAlpha(200);
        mPaintText.setTextSize(30);
        canvas.drawText(mColorString, mRect.left + 10, mRect.bottom - 10, mPaintText);

    }

}
