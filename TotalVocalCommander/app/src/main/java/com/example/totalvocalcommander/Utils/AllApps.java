package com.example.totalvocalcommander.Utils;
/*
    AllApps class used to inspect and retrieve all installed applications on the device
    that have a launch intent
 */

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;

import java.util.List;

public class AllApps {
    Context mContext;

    public AllApps(Context context) {
        mContext = context; // context needed for obtaining the package manager
    }

    /*
        launchApp method is used to launch an activity only if it has launch intent
        as parameter it receives and String parameter
     */
    public void launchApp(String appName) {
        final PackageManager pm = mContext.getPackageManager();
        // each time an application launch is requested the package manager is interogated for
        //launch intents as the application list might've been updated
        List<ApplicationInfo> packages = pm
                .getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            String applicationName = packageInfo.loadLabel(pm).toString().toLowerCase().trim();
            if (applicationName.equals(appName)) {
                startActivitySafe(packageInfo.packageName);
                return;
            }
        }
    }

    /*
        The method used to start the activity safely.
        The main launch method is surounded by a try/catch structure  to avoid exceptions throws.
        This scenario is impossible but we still cover it.
     */
    private void startActivitySafe(String packageName) {
        Intent mIntent = mContext.getPackageManager().getLaunchIntentForPackage(
                packageName);
        if (mIntent != null) {

            try {
                mContext.startActivity(mIntent);
            } catch (ActivityNotFoundException err) {
                Toast t = Toast.makeText(mContext,
                        "Application not found", Toast.LENGTH_SHORT);
                t.show();
            }
        }
    }

}
