package com.example.totalvocalcommander.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/*
    RectangleView, view object used by the OverlayService to highlight views
 */
public class VoiceAssistView extends View {
    Paint mPaintRectangle = new Paint();
    Rect mRect;
    int mColor = Color.YELLOW;

    public VoiceAssistView(Context context, Rect rect) {
        super(context);
        this.mRect = rect;

    }

    @Override
    public void onDraw(Canvas canvas) {
        mPaintRectangle.setColor(mColor);
        mPaintRectangle.setStyle(Paint.Style.STROKE);
        mPaintRectangle.setStrokeWidth(10);
        canvas.drawRect(mRect, mPaintRectangle);

    }

}
