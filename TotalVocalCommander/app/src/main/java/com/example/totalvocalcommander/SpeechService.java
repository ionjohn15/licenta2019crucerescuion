package com.example.totalvocalcommander;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import com.example.totalvocalcommander.Utils.SpeechServiceHelper;

import java.util.List;
import java.util.Locale;

import static android.speech.RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS;

public class SpeechService extends Service {

    private static final String TAG = SpeechService.class.getSimpleName();
    private static final int INSTANTIATION_TIME = 300;
    private Context mContext;

    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;
    private String mCommand;
    private boolean mErrorFlag = false;
    private SpeechServiceHelper mSpeechServiceHelper;
    private DataFromTTS mReceiver;
    private RecognitionListener mRecognitionListener = new RecognitionListener() {
        @Override
        public void onReadyForSpeech(Bundle params) {
        }

        @Override
        public void onBeginningOfSpeech() {

        }

        @Override
        public void onRmsChanged(float v) {

        }

        @Override
        public void onBufferReceived(byte[] bytes) {

        }

        @Override
        public void onEndOfSpeech() {
        }

        @Override
        public void onError(int error) {
            mErrorFlag = true;
            String message;
            switch (error) {
                case SpeechRecognizer.ERROR_AUDIO:
                    message = "Audio recording error";
                    break;
                case SpeechRecognizer.ERROR_CLIENT:
                    message = "Client side error";
                    break;
                case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                    message = "Insufficient permissions";
                    break;
                case SpeechRecognizer.ERROR_NETWORK:
                    message = "Network error";
                    break;
                case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                    message = "Network timeout";
                    break;
                case SpeechRecognizer.ERROR_NO_MATCH:
                    message = "No match";
                    restartCycle();
                    break;
                case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                    message = "RecognitionService busy";
                    break;

                case SpeechRecognizer.ERROR_SERVER:
                    message = "error from server";
                    break;
                case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                    message = "No speech input";
                    restartCycle();
                    break;
                default:
                    message = "Didn't understand, please try again.";

            }
            Log.e(TAG + "ERROR", message);
        }

        @Override
        public void onResults(Bundle results) {
            receiveResults(results);
            restartCycle();
            Log.d(TAG, "full results: " + mCommand);
            Toast.makeText(getApplicationContext(), mCommand, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onPartialResults(Bundle partialResults) {
        }

        @Override
        public void onEvent(int i, Bundle bundle) {

        }

        private void restartCycle() {
            if (!mErrorFlag)
                stopListening();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startListening();
                }
            }, INSTANTIATION_TIME);
            mErrorFlag = false;

        }

        private void receiveResults(Bundle results) {
            if ((results != null)
                    && results.containsKey(SpeechRecognizer.RESULTS_RECOGNITION)) {
                List<String> heard =
                        results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

                if (heard != null) {
                    mSpeechServiceHelper.analyzePartial(heard.get(0));
                    mCommand = heard.get(0);
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w(TAG, "onDestroy");
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.destroy();
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.w(TAG, "onCreate");
        mContext = this;
        mCommand = " ";
        mSpeechServiceHelper = new SpeechServiceHelper(this);
        initSpeechRecognizer();
        mReceiver = new DataFromTTS();
        registerReceiver(mReceiver, new IntentFilter("SWITCH_TTS"));
    }

    public void initSpeechRecognizer() {
        Log.w("ddebug", "initSpeechRecognizer");
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mContext);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault());
        mSpeechRecognizerIntent.putExtra(EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, 10000);
        mSpeechRecognizerIntent.putExtra("calling_package", "com.example.totalvocalcommander");
        AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_ALARM, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0, 0);
        mSpeechRecognizer.setRecognitionListener(mRecognitionListener);
        startListening();
    }

    public String getCommand() {
        Toast.makeText(mContext, mCommand, Toast.LENGTH_SHORT).show();
        return mCommand;
    }

    public void startListening() {
        Log.w(TAG, "startListening");
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }

    public void stopListening() {
        Log.w(TAG, "stopListening");
        mSpeechRecognizer.stopListening();
    }

    public void startListenerForTTS() {
        AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_ALARM, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
        amanager.setStreamVolume(AudioManager.STREAM_SYSTEM, 0, 0);
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
            Log.w(TAG,"startListenerForTTS");
        }
    }

    public void stopListenerForTTS() {
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.cancel();
            Log.w(TAG,"stopListenerForTTS");
        }
        AudioManager amanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        amanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, amanager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), 0);
        amanager.setStreamVolume(AudioManager.STREAM_ALARM, amanager.getStreamMaxVolume(AudioManager.STREAM_ALARM), 0);
        amanager.setStreamVolume(AudioManager.STREAM_MUSIC, amanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        amanager.setStreamVolume(AudioManager.STREAM_RING, amanager.getStreamMaxVolume(AudioManager.STREAM_RING), 0);
        amanager.setStreamVolume(AudioManager.STREAM_SYSTEM, amanager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM), 0);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class DataFromTTS extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("SWITCH_TTS")) {
                boolean mode = intent.getBooleanExtra("mode", true);
                if (mode) {
                    startListenerForTTS();
                } else {
                    stopListenerForTTS();
                }

            }
        }
    }


}
