package com.example.totalvocalcommander.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.totalvocalcommander.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import static com.example.totalvocalcommander.Utils.DecisionMaker.BACK_ACTION;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_CLEAR;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_ENTER;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_OFF;
import static com.example.totalvocalcommander.Utils.DecisionMaker.DICTATE_ON;
import static com.example.totalvocalcommander.Utils.DecisionMaker.HOME_ACTION;
import static com.example.totalvocalcommander.Utils.DecisionMaker.INVALID_VALUE;
import static com.example.totalvocalcommander.Utils.DecisionMaker.NOTIFICATIONS_ACTION;
import static com.example.totalvocalcommander.Utils.DecisionMaker.RECENT_ACTION;
import static com.example.totalvocalcommander.Utils.DecisionMaker.WORD_VALUE;

public class SpeechServiceHelper {
    private Context mContext;
    private List<String> mSingleDigits;
    private List<String> mTwoDigits;
    private List<String> mTenMultiple;
    private List<String> mGlobalActions;
    private List<String> mColors;
    private List<String> mDirections;
    private AllApps mAllApps;
    private boolean mForced = false;
    private String mPressWord;
    private boolean mLongPress = false;
    private String mColor = "none";
    private String mDirection = "none";

    public SpeechServiceHelper(Context context) {
        mContext = context;
        mSingleDigits = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.singleDigits)));
        mTwoDigits = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.twoDigits)));
        mTenMultiple = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.tensMultiple)));
        mGlobalActions = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.globalActions)));
        mColors = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.colorNames)));
        mDirections = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.globalDirections)));
        mPressWord = "";
        mAllApps = new AllApps(mContext);
    }

    /*
        Function that parses the String received from the the SpeechService service
     */
    public void analyzePartial(String partial) {
        Intent sendLevel = new Intent();
        sendLevel.setAction("PRESS_DATA");
        int value = INVALID_VALUE;

        StringTokenizer t = new StringTokenizer(partial);
        String word = "";

        List<String> sentence = new ArrayList<>();
        while (t.hasMoreTokens()) {
            word = t.nextToken();
            sentence.add(word.toLowerCase());
        }

        if (sentence.contains("press")) {
            value = getPressValue(sentence);
        }

        if (sentence.contains("dictate")) {
            value = getDictateValue(sentence);
        }

        if (sentence.contains("open")) {
            openApp(sentence);
        }

        if (sentence.contains("gesture") && sentence.contains("try")) {
            tryGesture(sentence);
        }

        if (sentence.contains("scroll")) {
            value = getScrollValue(sentence);
        }

        if (sentence.contains("voice") && sentence.contains("turn") && sentence.contains("assist")) {
            sendVoiceAssistMode(sentence);
        }
        if (sentence.contains("advanced") && sentence.contains("turn") && sentence.contains("mode")) {
            sendAdvancedMode(sentence);
        }

        if (sentence.contains("learn") && sentence.contains("turn") && sentence.contains("mode")) {
            sendLearnMode(sentence);
        }

        if (sentence.contains("save") && sentence.contains("gesture")) {
            saveGesture(sentence);
        }

        if (value != INVALID_VALUE) {
            sendLevel.putExtra("view_value", value);
            sendLevel.putExtra("direction", mDirection);
            sendLevel.putExtra("color", mColor);
            sendLevel.putExtra("forced", mForced);
            sendLevel.putExtra("long_press", mLongPress);
            sendLevel.putExtra("press_word", mPressWord);
        }
        sendLevel.putExtra("string_data", partial);
        mContext.sendBroadcast(sendLevel);
        resetData();
    }

    /*
        Reset all data received after parsing
     */
    private void resetData() {
        mDirection = "none";
        mColor = "none";
        mForced = false;
        mLongPress = false;
        mPressWord = "";
    }

    /*
        Open app command is processed directly AllApps as it is a simple action
     */
    private void openApp(List<String> sentence) {
        sentence.remove("open");
        String result = "";
        for (int i = 0; i < sentence.size(); i++) {
            result += sentence.get(i) + " ";
        }
        result = result.trim();
        mAllApps.launchApp(result);

    }

    /*
        This method extracts data for the scroll action
     */
    private int getScrollValue(List<String> sentence) {
        sentence.remove("scroll");
        if (sentence.contains("force")) {
            mForced = true;
            sentence.remove("force");
        }


        if (sentence.size() == 2) {
            String firstValue = sentence.get(0);
            String secondValue = sentence.get(1);

            if (isColor(firstValue) || isColor(secondValue)) {
                if (isColor(firstValue)) {
                    mColor = firstValue;
                } else {
                    mColor = secondValue;
                }
            } else {
                return INVALID_VALUE;
            }

            if (isDirection(firstValue) || isDirection(secondValue)) {
                if (isDirection(firstValue)) {
                    mDirection = firstValue;
                } else {
                    mDirection = secondValue;
                }
            } else {
                return INVALID_VALUE;
            }

            return 1000;
        }
        return INVALID_VALUE;
    }

    /*
        Method that extracts data for the Advanced Mode action
     */
    private void sendAdvancedMode(List<String> sentence) {
        sentence.remove("advanced");
        sentence.remove("turn");
        sentence.remove("mode");

        if (sentence.size() == 1) {
            String firstValue = sentence.get(0);
            if (firstValue.equals("on")) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("ADVANCED_MODE");
                sendLevel.putExtra("mode", true);
                mContext.sendBroadcast(sendLevel);
            }

            if (firstValue.equals("off")) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("ADVANCED_MODE");
                sendLevel.putExtra("mode", false);
                mContext.sendBroadcast(sendLevel);
            }


        }
    }

    private void sendVoiceAssistMode(List<String> sentence) {
        sentence.remove("voice");
        sentence.remove("turn");
        sentence.remove("assist");

        if (sentence.size() == 1) {
            String firstValue = sentence.get(0);
            if (firstValue.equals("on")) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("VOICE_ASSIST");
                sendLevel.putExtra("voice_mode", true);
                mContext.sendBroadcast(sendLevel);
            }

            if (firstValue.equals("off")) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("VOICE_ASSIST");
                sendLevel.putExtra("voice_mode", false);
                mContext.sendBroadcast(sendLevel);
            }


        }
    }

    /*
        Comunicates with overlay service to turn on/off Learn Mode
     */
    private void sendLearnMode(List<String> sentence) {
        sentence.remove("learn");
        sentence.remove("turn");
        sentence.remove("mode");

        if (sentence.size() == 1) {
            String firstValue = sentence.get(0);
            if (firstValue.equals("on")) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("LEARN_MODE");
                sendLevel.putExtra("mode", true);
                mContext.sendBroadcast(sendLevel);
            }

            if (firstValue.equals("off")) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("LEARN_MODE");
                sendLevel.putExtra("mode", false);
                mContext.sendBroadcast(sendLevel);
            }
        }
    }

    /*
        As the gesture data is saved in a Shared Preference we can read all the data directly and
        send it to the executive service
     */
    private void tryGesture(List<String> sentence) {
        sentence.remove("gesture");
        sentence.remove("try");
        String result = "";
        for (int i = 0; i < sentence.size(); i++) {
            result += sentence.get(i) + " ";
        }
        result = result.trim();
        SharedPreferences sharedPref = mContext.getSharedPreferences("GESTURE_DATA", Context.MODE_PRIVATE);
        String dataForGesture = sharedPref.getString(result, null);
        if (dataForGesture != null) {
            Gson gson = new Gson();
            EventModel items[] = gson.fromJson(dataForGesture, EventModel[].class);
            Log.w("ddebug", items.length + "");
            EventModel lastItem = null;
            for (EventModel item : items) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("LEARN_MODE_DISPATCH");
                sendLevel.putExtra("x", item.getX());
                sendLevel.putExtra("y", item.getY());
                sendLevel.putExtra("time", item.getTotalTime());
                sendLevel.putExtra("is_tap", item.getIsTap());
                mContext.sendBroadcast(sendLevel);
                lastItem = item;
            }
            if (lastItem != null) {
                Intent sendLevel = new Intent();
                sendLevel.setAction("TRY_GESTURE");
                sendLevel.putExtra("end", true);
                mContext.sendBroadcast(sendLevel);
            }

        }

    }

    /*
        Saves a gesture data to Shared Preferences
     */
    private void saveGesture(List<String> sentence) {
        Log.w("ddebug", "SAVE GESTURE");
        sentence.remove("save");
        sentence.remove("gesture");
        String result = "";
        for (int i = 0; i < sentence.size(); i++) {
            result += sentence.get(i) + " ";
        }
        result = result.trim();
        if (sentence.size() != 0) {
            Intent sendLevel = new Intent();
            sendLevel.setAction("LEARN_MODE");
            sendLevel.putExtra("mode", false);
            sendLevel.putExtra("gesture_name", result);
            mContext.sendBroadcast(sendLevel);
        }
    }

    private boolean isColor(String text) {
        return mColors.contains(text);
    }

    private boolean isDirection(String text) {
        return mDirections.contains(text);
    }

    /*
        Extracts a number value based on String values extracted from the Speech Recognizer
     */
    private int getPressValue(List<String> sentence) {
        sentence.remove("press");
        sentence.remove("number");
        if (sentence.contains("long")) {
            mLongPress = true;
            sentence.remove("long");
        }
        if (sentence.size() == 1) {
            String word = sentence.get(0);
            if (mGlobalActions.contains(sentence.get(0))) {
                return getGlobalAction(word);
            } else {
                int valueFromSingleString = getValueFromSingleString(word);
                if (valueFromSingleString != INVALID_VALUE) {
                    return valueFromSingleString;
                } else {
                    mPressWord = word.toLowerCase();
                    return WORD_VALUE;
                }

            }
        }

        if (sentence.size() == 2) {
            int firstValue = getValueFromSingleString(sentence.get(0));
            int secondValue = getValueFromSingleString(sentence.get(1));
            if (firstValue != INVALID_VALUE && secondValue != INVALID_VALUE) {
                return firstValue + secondValue;
            } else {
                mPressWord = sentence.get(0) + " " + sentence.get(1);
                return WORD_VALUE;
            }
        }
        for (int i = 0; i < sentence.size(); i++) {
            mPressWord = sentence.get(i) + " ";
        }
        mPressWord = mPressWord.trim();
        return WORD_VALUE;
    }

    /*
        Converts string to global value
     */
    private int getDictateValue(List<String> sentence) {
        sentence.remove("dictate");
        if (sentence.size() == 1) {
            String word = sentence.get(0);
            if (word.equals("on")) {
                return DICTATE_ON;
            }
            if (word.equals("off")) {
                return DICTATE_OFF;
            }

            if (word.equals("clear")) {
                return DICTATE_CLEAR;
            }

            if (word.equals("enter")) {
                return DICTATE_ENTER;
            }
        }
        return INVALID_VALUE;
    }

    private int getGlobalAction(String text) {
        switch (text.toLowerCase()) {
            case "back":
                return BACK_ACTION;
            case "home":
                return HOME_ACTION;
            case "recent":
                return RECENT_ACTION;
            case "notifications":
                return NOTIFICATIONS_ACTION;
            default:
                return INVALID_VALUE;
        }
    }

    /*
        Tries to return a number form andorid api parser, in case of error extracts the number
        from the string value
     */
    private int getValueFromSingleString(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return getValueOfTheString(value);
        }
    }

    private int getValueOfTheString(String value) {
        if (mSingleDigits.contains(value)) {
            return mSingleDigits.indexOf(value);
        }
        if (mTwoDigits.contains(value)) {
            return mTwoDigits.indexOf(value) + 10;
        }
        if (mTenMultiple.contains(value)) {
            return mTenMultiple.indexOf(value) * 10;
        }

        return INVALID_VALUE;
    }


}
