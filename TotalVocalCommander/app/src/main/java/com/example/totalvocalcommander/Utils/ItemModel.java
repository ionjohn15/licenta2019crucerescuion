package com.example.totalvocalcommander.Utils;

import android.graphics.Rect;
import android.view.accessibility.AccessibilityNodeInfo;

/*
    Item model that is used to store the most important data (position and view content)about a view object
    that is visibile and clickable on the device screen
 */
public class ItemModel {
    private AccessibilityNodeInfo mNodeInfo;
    private Rect mRect;

    public ItemModel(AccessibilityNodeInfo nodeInfo, Rect rect) {
        mNodeInfo = nodeInfo;
        mRect = rect;
    }

    public int getX() {
        return mRect.centerX();
    }

    public int getY() {
        return mRect.centerY();
    }

    /*
        Returns a confirmation if the current model has the title or label conform to the String parameter
     */
    public boolean containsString(String value) {
        String contentDescriptor = mNodeInfo.getContentDescription() != null ? mNodeInfo.getContentDescription().toString().toLowerCase() : "";
        String text = mNodeInfo.getText() != null ? mNodeInfo.getText().toString().toLowerCase() : "";
        if (contentDescriptor.isEmpty() && text.isEmpty()) {
            boolean result = false;
            for (int i = 0; i < mNodeInfo.getChildCount(); i++) {
                result = containsStringDeepSearch(mNodeInfo.getChild(i), value);
                if (result) {
                    return result;
                }
            }
        }
        return contentDescriptor.contains(value) || text.contains(value);
    }

    public boolean containsStringDeepSearch(AccessibilityNodeInfo nodeInfo, String value) {
        String contentDescriptor = nodeInfo.getContentDescription() != null ? nodeInfo.getContentDescription().toString().toLowerCase() : "";
        String text = nodeInfo.getText() != null ? nodeInfo.getText().toString().toLowerCase() : "";
        return contentDescriptor.contains(value) || text.contains(value);
    }

    public String getContentDescritor() {
        String contentDescriptor = mNodeInfo.getContentDescription() != null ? mNodeInfo.getContentDescription().toString().toLowerCase() : "";
        String text = mNodeInfo.getText() != null ? mNodeInfo.getText().toString().toLowerCase() : "";
        if (contentDescriptor.isEmpty() && text.isEmpty()) {
            String result = "";
            for (int i = 0; i < mNodeInfo.getChildCount(); i++) {
                result += deepSearch(mNodeInfo.getChild(i)) + " ";

            }
            if (!result.isEmpty()) {
                return result;
            }
        }
        return !contentDescriptor.isEmpty() ? contentDescriptor : text;
    }

    public String deepSearch(AccessibilityNodeInfo nodeInfo) {
        String contentDescriptor = nodeInfo.getContentDescription() != null ? nodeInfo.getContentDescription().toString().toLowerCase() : "";
        String text = nodeInfo.getText() != null ? nodeInfo.getText().toString().toLowerCase() : "";
        return !contentDescriptor.isEmpty() ? contentDescriptor : text;
    }


    public Rect getRect() {
        return mRect;
    }

    @Override
    public boolean equals(Object obj) {
        ItemModel other = (ItemModel) obj;
        if (this.mRect.equals(other.mRect)) {
            return true;
        } else {
            return false;
        }
    }

}
