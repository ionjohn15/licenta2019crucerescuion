package com.example.totalvocalcommander;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.WindowManager;

import com.example.totalvocalcommander.Utils.OverlayServiceHelper;

/*
    Overlay service that has permissions to draw over system objects
 */
public class OverlayService extends Service {
    private final static int INVALID_VALUE = -10000;
    private DataForOverlayReceiver mReceiver;
    private WindowManager mWindowManager;
    private int mCurrentIdNumbers;
    private int mCurrentIdScrolls;
    private OverlayServiceHelper mOverlayServiceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mOverlayServiceHelper = new OverlayServiceHelper(this, mWindowManager);
        mReceiver = new DataForOverlayReceiver();
        mCurrentIdNumbers = 0;
        mCurrentIdScrolls = 0;
        registerReceiver(mReceiver, new IntentFilter("OBJECTS_DATA"));
        registerReceiver(mReceiver, new IntentFilter("SCROLLABLE_DATA"));
        registerReceiver(mReceiver, new IntentFilter("ADVANCED_MODE"));
        registerReceiver(mReceiver, new IntentFilter("LEARN_MODE"));
        registerReceiver(mReceiver, new IntentFilter("FROM_TTS_DATA"));
        registerReceiver(mReceiver, new IntentFilter("FROM_TTS_RESET"));
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    /*
        Receiver that listens for the data to be displayed
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    class DataForOverlayReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("ADVANCED_MODE")) {
                boolean mode = intent.getBooleanExtra("mode", false);
                if (mode) {
                    mOverlayServiceHelper.setAdvancedMode(true);
                    mOverlayServiceHelper.showImageViews();
                    mOverlayServiceHelper.showScrollViews();
                    Log.w("ddebug", "show");
                } else {
                    mOverlayServiceHelper.setAdvancedMode(false);
                    mOverlayServiceHelper.hideImageViews();
                    mOverlayServiceHelper.hideScrollViews();
                    Log.w("ddebug", "hide");


                }
            }

            if (intent.getAction().equals("LEARN_MODE")) {
                boolean mode = intent.getBooleanExtra("mode", false);
                String gestureName = intent.getStringExtra("gesture_name");
                if (mode) {
                    mOverlayServiceHelper.addGesturelayer();
                } else {
                    if (gestureName != null && !gestureName.isEmpty()) {
                        mOverlayServiceHelper.saveGesture(gestureName);
                    } else {
                        mOverlayServiceHelper.deleteOverlay();
                    }
                }
            }
            if (intent.getAction().equals("OBJECTS_DATA")) {
                int X = intent.getIntExtra("X", INVALID_VALUE);
                int Y = intent.getIntExtra("Y", INVALID_VALUE);
                int id = intent.getIntExtra("id", INVALID_VALUE);

                if (mCurrentIdNumbers != id) {
                    mOverlayServiceHelper.resetImageViews();
                    mCurrentIdNumbers = id;
                }
                String XY = String.valueOf(X) + String.valueOf(Y);
                if (!mOverlayServiceHelper.mapContains(XY)) {
                    mOverlayServiceHelper.addXYToMap(XY);
                    mOverlayServiceHelper.updateOverlayClickable(X, Y);
                }

            }

            if (intent.getAction().equals("SCROLLABLE_DATA")) {
                int id = intent.getIntExtra("id", INVALID_VALUE);
                Rect r = intent.getParcelableExtra("r");
                String colorString = intent.getStringExtra("colorString");
                int color = INVALID_VALUE;
                if (colorString != null)
                    color = Color.parseColor(colorString);
                if (mCurrentIdScrolls != id) {
                    mOverlayServiceHelper.resetScrollViews();
                    mCurrentIdScrolls = id;
                }
                if (r != null && color != INVALID_VALUE) {
                    mOverlayServiceHelper.updateOverlayScrollable(r, color, colorString);
                }
            }

            if (intent.getAction().equals("FROM_TTS_DATA")) {
                Rect r = intent.getParcelableExtra("rect");
                mOverlayServiceHelper.updateOverlayTTSData(r);
            }

            if (intent.getAction().equals("FROM_TTS_RESET")) {
                mOverlayServiceHelper.removeLastRectangleTTS();
            }


        }
    }
}
