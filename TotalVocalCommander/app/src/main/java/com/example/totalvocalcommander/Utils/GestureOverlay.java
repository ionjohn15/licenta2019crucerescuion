package com.example.totalvocalcommander.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/*
    GestureOverlay represents a view that is add on top of all views
    to register all the MotionEvents produced by the user
 */
public class GestureOverlay extends View implements
        GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {
    private final static String DEBUG_TAG = "GestureOverlay";

    Context mContext;
    List<EventModel> mEvents;
    long startTime;
    long elapseTime;
    boolean firstFlag = false;
    private GestureDetectorCompat mDetector;
    Paint mPaintRectangle = new Paint();
    Rect mRect = new Rect();


    public GestureOverlay(Context context) {
        super(context);
        mContext = context;
        mEvents = new ArrayList<>();
        mDetector = new GestureDetectorCompat(context, this);
        // Set the gesture detector as the double tap
        // listener.
        mDetector.setOnDoubleTapListener(this);
        startTime = System.currentTimeMillis();
        ;
        elapseTime = 0;


    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.scale(1, 1);
        mPaintRectangle.setColor(Color.GREEN);
        mPaintRectangle.setStyle(Paint.Style.FILL);
        mPaintRectangle.setStrokeWidth(10);
        canvas.getClipBounds(mRect);
        canvas.drawRect(mRect, mPaintRectangle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            startTime = System.currentTimeMillis();
            firstFlag = true;
        }

        elapseTime = System.currentTimeMillis() - startTime;


        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    /*
        Stores all the recorded data to a shared preference while the overlay was active
     */
    public void saveEvents(String name) {
        Gson gson = new Gson();
        String eventListToString = gson.toJson(mEvents);
        SharedPreferences sharedPref = mContext.getSharedPreferences("GESTURE_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(name, eventListToString);
        editor.apply();
    }

    public void clear() {
        mEvents.clear();
    }

    @Override
    public boolean onDown(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDown: " + event.toString());
        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {
        Log.d(DEBUG_TAG, "onFling: " + event1.toString() + event2.toString());
        mEvents.add(new EventModel(event2.getRawX(), event2.getRawY(), elapseTime, false));
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
        mEvents.add(new EventModel(event.getRawX(), event.getRawY(), elapseTime, true));
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX,
                            float distanceY) {
        Log.d(DEBUG_TAG, "onScroll: " + event1.toString() + event2.toString());
        mEvents.add(new EventModel(event2.getRawX(), event2.getRawY(), elapseTime, false));
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
        Log.d(DEBUG_TAG, "onShowPress: " + event.toString());
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapUp: " + event.toString());
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTap: " + event.toString());
        mEvents.add(new EventModel(event.getRawX(), event.getRawY(), elapseTime, true));
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
        Log.d(DEBUG_TAG, "onDoubleTapEvent: " + event.toString());
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
        Log.d(DEBUG_TAG, "onSingleTapConfirmed: " + event.toString());
        mEvents.add(new EventModel(event.getRawX(), event.getRawY(), elapseTime, true));
        return true;
    }

}